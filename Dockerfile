FROM ubuntu:21.10

RUN apt-get update

RUN DEBIAN_FRONTEND=noninteractive TZ=Asia/Kolkata apt-get -y install tzdata
RUN apt-get install -y neovim zsh git

RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git /opt/.zsh/zsh-syntax-highlighting
RUN git clone https://github.com/zsh-users/zsh-autosuggestions /opt/.zsh/zsh-autosuggestions
RUN chsh -s /usr/bin/zsh

COPY .zshrc /.zshrc
CMD rm -rf /root/.zshrc /root/.zsh && mv -T /.zshrc root/.zshrc && mv -T /opt/.zsh /root/.zsh && cd /root && /usr/bin/zsh
