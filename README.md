# Pwnbox

A minimal ubuntu docker container for binary exploitation.

- zsh
- gdb
- gdbserver
- pwntools
- neovim

## Running

`docker run --rm -it --name pwnbox -v ~/pwn:/root pwnbox`
